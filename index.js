require('dotenv').config()
const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose');
const productsRoutes = require('./routes/productsRoutes')
const categoryRoutes = require('./routes/categoryRoutes')
const port = 3000

app.use (cors())
app.use (express.json())
app.use('/products', productsRoutes)
app.use('/category', categoryRoutes)



main().then(() => console.log("connected")).catch(err => console.log(err));

async function main() {
  const url = process.env.DB_URL
  const password = process.env.DB_PASSWORD
  const UrlWithPassword = url.replace('<password>', password)
  await mongoose.connect(UrlWithPassword)    

  // use `await mongoose.connect('mongodb://user:password@127.0.0.1:27017/test');` if your database has auth enabled
}


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})