const Product = require("../models/productsModel");

const getAllProducts = async(req, res) =>{
    try{
       const products = await Product.find({});
       res.status(200).json(products)

    }catch(error){
        res.status(500).send("Error occured")
    }
}

const getProductById = async(req, res) => {
    try{
        const product = await Product.findById(req.params.productId).exec();
        res.status(200).json(product)
    }catch(error){
        res.status(404).send("Product of given ID not found")
    }
}

const addProduct = async(req, res) =>{
    try{
        const product = new Product(req.body)
        await product.save()
        res.status(201).json(product)
    }catch(error){
        res.status(400).send("Please check Product fields")
    }
}

const updateProduct = async(req, res) =>{
    try{
        const updatedProduct = await Product.findByIdAndUpdate(req.params.productId, req.body, {new:true})
        res.status(200).json(updatedProduct)
    }catch(error){
        res.status(400).send("Please check Product filelds")
    }
}

const deleteProduct = async(req, res) =>{
    try{
        await Product.findByIdAndDelete(req.params.productId)
        res.status(204).send("deleted")
    }catch(error){
        res.status(404).send("Product of given ID not found")
    }
}


module.exports = {
    getAllProducts,
    getProductById,
    addProduct,
    updateProduct,
    deleteProduct
}