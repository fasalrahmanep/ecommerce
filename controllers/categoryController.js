const Category = require("../models/categoryModel");

const getAllCategory = async(req, res) => {
    try{
       const categories = await Category.find({});
       res.status(200).json(categories)
    }catch(error){
        res.status(500).send("Error occured")
    }
}

const getCategoryById =  async(req, res) => {
    try{
        const category = await Category.findById(rq.params.categoryId).exec();
        res.status(200).json(category)
    }catch(error){
        res.status(404).send("Category given ID not found")
    }
}

const addCategory =  async(req, res) => {
    try{
        const category = new Category(req.body)
        await category.save();
        res.status(201).json(category)
    }catch(error){
        res.status(400).send("please check category fields")
    }
}

const updateCategory = async(req, res) => {
    try{
        const updatedCategory = await Category.findByIdAndUpdate(req.params.categoryId, req.body, {new:true})
        res.status(200).json(updatedCategory)
    }catch(error){
        res.status(400).send("Please check category fields")
    }
}

const deleteCategory =  async(req, res) => {
    try{
        await Category.findByIdAndDelete(req.params.categoryId)
        res.status(204).send("deleted")
    }catch(error){

        res.status(404).send("Category given ID not found")
    }
}

module.exports = {
    getAllCategory,
    getCategoryById,
    addCategory,
    updateCategory,
    deleteCategory
}