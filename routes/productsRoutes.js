const express = require('express')
const { getAllProducts, addProduct, getProductById, updateProduct, deleteProduct } = require('../controllers/productsControllers')
const router = express.Router()

router.get('/', getAllProducts)

router.get('/:productId', getProductById)

router.post('/', addProduct)

router.patch('/:productId', updateProduct)

router.delete('/:productId', deleteProduct)


module.exports = router