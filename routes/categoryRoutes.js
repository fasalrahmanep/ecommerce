const express = require('express')
const { addCategory, getAllCategory, getCategoryById, updateCategory, deleteCategory } = require('../controllers/categoryController')
const router = express.Router()

router.get('/', getAllCategory)

router.get('/:categoryId', getCategoryById)

router.post('/', addCategory)

router.patch('/:categoryId', updateCategory)

router.delete('/:categoryId', deleteCategory)


module.exports = router